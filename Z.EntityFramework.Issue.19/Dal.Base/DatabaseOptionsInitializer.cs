using System;
using System.Data.Entity;

namespace Dal.Base
{
    public class DatabaseOptionsInitializer<T> : IDatabaseInitializer<T> where T : DbContext, new()
    {
        public DatabaseOptionsInitializer(Action<T> seed = null)
        {
            Seed = seed ?? delegate { };
        }

        public Action<T> Seed { get; set; }

        public void InitializeDatabase(T context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();
            }

            context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "ALTER DATABASE [" + context.Database.Connection.Database + "] SET ALLOW_SNAPSHOT_ISOLATION ON");
            context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "ALTER DATABASE [" + context.Database.Connection.Database + "] SET ALLOW_SNAPSHOT_ISOLATION ON");
			
            Seed(context);
        }
    }
}