#pragma warning disable 1591          

using System;
using System.CodeDom.Compiler;

namespace Dal.Base.Common
{
	public class Item
	{
		public Item()
		{
			IsPackage = false;
		}

		public Guid Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public bool IsPackage { get; set; }

		public override string ToString()
		{
			return Code + " - " + Name;
		}
	}
}