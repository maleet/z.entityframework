using System.Data.Entity.ModelConfiguration;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Dal.Base.Common {

    public partial class ItemConfiguration : EntityTypeConfiguration<Item>
    {
        public ItemConfiguration()
            : this("dbo")
        {
        }
 
        public ItemConfiguration(string schema)
        {
			ToTable(schema + ".Item");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("uniqueidentifier").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasColumnType("nvarchar").HasMaxLength(128);
            Property(x => x.IsPackage).HasColumnName("IsPackage").IsRequired().HasColumnType("bit");

			InitializePartial();
        }
        partial void InitializePartial();
    }

	}

