﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Manufacturing;
using Nortal.Mes.Dal.Manufacturing.Entities;
using Task = Nortal.Mes.Dal.Manufacturing.Entities.Task;

namespace Z.EntityFramework.Issue._19
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var entities = new MesEntities())
            {
                var firstOrDefault = entities.FinishedItems
                    .Include(item => item.MasterOperations)
                    .FirstOrDefault();

                var newCopy = new MasterOperation()
                {
                    Id = Guid.NewGuid(),
                    Item = firstOrDefault,
                    Parent = firstOrDefault.MasterOperations.First(),
                    Code = "M2",
                    ProcessNumber = 5
                };

                var productionOrder = new ProductionOrder()
                {
                    Id = Guid.NewGuid(),
                    Code = "P1",
                    Item = firstOrDefault,
                    MasterOperations = new List<MasterOperation>()
                    {
                        newCopy,
                        new MasterOperation()
                        {
                            Id = Guid.NewGuid(),
                            Code = "M3",
                            ProcessNumber = 6
                        },
                    },
                    Operations = new List<Operation>()
                    {
                        new Operation()
                        {
                            MasterOperation = newCopy,
                            Id = Guid.NewGuid(),
                            Children = new List<Task>()
                            {
                                new ProductionTask()
                                {
                                    Id = Guid.NewGuid(),
                                    StartTime = DateTime.Now
                                }
                            }
                        }
                    }
                };

                entities.ProductionOrders.Add(productionOrder);
                entities.BulkSaveChanges(false);
            }
            using (var entities = new MesEntities())
            {
                var productionOrders = entities.ProductionOrders
                    .Include(order => order.MasterOperations)
                    .Include(order => order.Operations.Select(operation => operation.Children))
                    .ToList();

                entities.Tasks.RemoveRange(productionOrders.SelectMany(order => order.Operations.SelectMany(operation => operation.Children)));
                entities.Operations.RemoveRange(productionOrders.SelectMany(order => order.Operations));
                entities.MasterOperations.RemoveRange(productionOrders.SelectMany(order => order.MasterOperations));
                entities.ProductionOrders.RemoveRange(productionOrders);

                entities.BulkSaveChanges(false);
            }

            Console.ReadKey();
        }
    }
}