namespace Dal.Manufacturing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModelCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Item",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Code = c.String(),
                        Name = c.String(maxLength: 128),
                        ItemTypeClv = c.Guid(nullable: false),
                        ItemGroupId = c.Guid(nullable: false),
                        TaxId = c.Guid(nullable: false),
                        InventoryUnitId = c.Guid(nullable: false),
                        StorageUnitId = c.Guid(),
                        LotControlClv = c.Guid(nullable: false),
                        IsPackage = c.Boolean(nullable: false),
                        OriginCountryId = c.Guid(),
                        EanCode = c.Long(),
                        Weight = c.Decimal(nullable: false, precision: 12, scale: 4, storeType: "numeric"),
                        WeightUnitId = c.Guid(nullable: false),
                        Volume = c.Decimal(nullable: false, precision: 12, scale: 4, storeType: "numeric"),
                        VolumeUnitId = c.Guid(nullable: false),
                        SalesUnitId = c.Guid(nullable: false),
                        PurchaseUnitId = c.Guid(nullable: false),
                        OutboundMethodClv = c.Guid(nullable: false),
                        OrderSystemClv = c.Guid(),
                        OrderMethodClv = c.Guid(nullable: false),
                        OrderQuantityIncrement = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        MinimumOrderQuantity = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        MaximumOrderQuantity = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        FixedOrderQuantity = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        EconomicOrderQuantity = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        ReorderPoint = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        SafetyStock = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        OrderIntervalDays = c.Int(),
                        OrderLeadTimeDays = c.Int(),
                        SafetyTimeDays = c.Int(),
                        IsPurchaseInspectionNeeded = c.Boolean(nullable: false),
                        AbcCode = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        SlowMovingPercent = c.Decimal(precision: 12, scale: 4, storeType: "numeric"),
                        ShelfLifePeriodClv = c.Guid(),
                        ShelfLife = c.Int(nullable: false),
                        SignalId = c.Guid(),
                        Comment = c.String(maxLength: 4000),
                        DefaultDetailHeaderId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Manufacturing.MasterOperation",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.Guid(nullable: false),
                        Code = c.String(nullable: false, maxLength: 32),
                        ParentId = c.Guid(),
                        ProductionOrderId = c.Guid(),
                        ProcessId = c.Guid(nullable: false),
                        ProcessNumber = c.Int(nullable: false),
                        ItemId = c.Guid(),
                        ItemGroupId = c.Guid(),
                        ProcessSelectorId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Manufacturing.Item", t => t.ItemId)
                .ForeignKey("Manufacturing.MasterOperation", t => t.ParentId)
                .ForeignKey("Manufacturing.ProductionOrder", t => t.ProductionOrderId)
                .Index(t => t.ParentId)
                .Index(t => t.ProductionOrderId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "Manufacturing.ProductionOrder",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Code = c.String(maxLength: 32),
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Manufacturing.Item", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "Manufacturing.Operation",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Code = c.String(maxLength: 4000),
                        OrderId = c.Guid(),
                        MasterOperationId = c.Guid(),
                        ParentId = c.Guid(),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Manufacturing.Operation", t => t.ParentId)
                .ForeignKey("Manufacturing.MasterOperation", t => t.MasterOperationId)
                .ForeignKey("Manufacturing.ProductionOrder", t => t.OrderId)
                .Index(t => t.OrderId)
                .Index(t => t.MasterOperationId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "Manufacturing.Item",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PurchaseLeadTime = c.Double(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Item", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Manufacturing.Item", "Id", "dbo.Item");
            DropForeignKey("Manufacturing.MasterOperation", "ProductionOrderId", "Manufacturing.ProductionOrder");
            DropForeignKey("Manufacturing.MasterOperation", "ParentId", "Manufacturing.MasterOperation");
            DropForeignKey("Manufacturing.MasterOperation", "ItemId", "Manufacturing.Item");
            DropForeignKey("Manufacturing.Operation", "OrderId", "Manufacturing.ProductionOrder");
            DropForeignKey("Manufacturing.Operation", "MasterOperationId", "Manufacturing.MasterOperation");
            DropForeignKey("Manufacturing.Operation", "ParentId", "Manufacturing.Operation");
            DropForeignKey("Manufacturing.ProductionOrder", "ItemId", "Manufacturing.Item");
            DropIndex("Manufacturing.Item", new[] { "Id" });
            DropIndex("Manufacturing.Operation", new[] { "ParentId" });
            DropIndex("Manufacturing.Operation", new[] { "MasterOperationId" });
            DropIndex("Manufacturing.Operation", new[] { "OrderId" });
            DropIndex("Manufacturing.ProductionOrder", new[] { "ItemId" });
            DropIndex("Manufacturing.MasterOperation", new[] { "ItemId" });
            DropIndex("Manufacturing.MasterOperation", new[] { "ProductionOrderId" });
            DropIndex("Manufacturing.MasterOperation", new[] { "ParentId" });
            DropTable("Manufacturing.Item");
            DropTable("Manufacturing.Operation");
            DropTable("Manufacturing.ProductionOrder");
            DropTable("Manufacturing.MasterOperation");
            DropTable("dbo.Item");
        }
    }
}
