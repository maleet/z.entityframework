using System.Collections.Generic;
using Dal.Base.Common;
using System;
using System.Data.Entity.Migrations;

namespace Dal.Manufacturing.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<MesEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MesEntities context)
        {
            context.Items.AddOrUpdate(item => item.Id, new Item()
            {
                Code = "Item1",
                Id = Guid.Parse("416D7DD1-7E5D-4E10-80E6-6A6D0B6E8427")
            });

            var finishedItem = new FinishedItem()
            {
                Code = "Item1",
                PurchaseLeadTime = 3,
                Id = Guid.Parse("5A398206-4716-482C-AA26-A6403B5F2DA0")
            };
            context.FinishedItems.AddOrUpdate(item => item.Id, finishedItem);

            var masterOperation = new MasterOperation()
            {
                Id = Guid.Parse("0E0F4C64-7D82-461A-A36F-A777DC26C2CB"),
                Code = "M",
                ProcessNumber = 34,
                ItemId = finishedItem.Id
            };
            context.MasterOperations.AddOrUpdate(operation => operation.Id, masterOperation);
        }
    }
}