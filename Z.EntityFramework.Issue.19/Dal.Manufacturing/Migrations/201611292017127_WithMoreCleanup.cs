namespace Dal.Manufacturing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WithMoreCleanup : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Manufacturing.MasterOperation", "ProcessId", c => c.Guid());
            DropColumn("dbo.Item", "ItemTypeClv");
            DropColumn("dbo.Item", "ItemGroupId");
            DropColumn("dbo.Item", "TaxId");
            DropColumn("dbo.Item", "InventoryUnitId");
            DropColumn("dbo.Item", "StorageUnitId");
            DropColumn("dbo.Item", "LotControlClv");
            DropColumn("dbo.Item", "OriginCountryId");
            DropColumn("dbo.Item", "EanCode");
            DropColumn("dbo.Item", "Weight");
            DropColumn("dbo.Item", "WeightUnitId");
            DropColumn("dbo.Item", "Volume");
            DropColumn("dbo.Item", "VolumeUnitId");
            DropColumn("dbo.Item", "SalesUnitId");
            DropColumn("dbo.Item", "PurchaseUnitId");
            DropColumn("dbo.Item", "OutboundMethodClv");
            DropColumn("dbo.Item", "OrderSystemClv");
            DropColumn("dbo.Item", "OrderMethodClv");
            DropColumn("dbo.Item", "OrderQuantityIncrement");
            DropColumn("dbo.Item", "MinimumOrderQuantity");
            DropColumn("dbo.Item", "MaximumOrderQuantity");
            DropColumn("dbo.Item", "FixedOrderQuantity");
            DropColumn("dbo.Item", "EconomicOrderQuantity");
            DropColumn("dbo.Item", "ReorderPoint");
            DropColumn("dbo.Item", "SafetyStock");
            DropColumn("dbo.Item", "OrderIntervalDays");
            DropColumn("dbo.Item", "OrderLeadTimeDays");
            DropColumn("dbo.Item", "SafetyTimeDays");
            DropColumn("dbo.Item", "IsPurchaseInspectionNeeded");
            DropColumn("dbo.Item", "AbcCode");
            DropColumn("dbo.Item", "SlowMovingPercent");
            DropColumn("dbo.Item", "ShelfLifePeriodClv");
            DropColumn("dbo.Item", "ShelfLife");
            DropColumn("dbo.Item", "SignalId");
            DropColumn("dbo.Item", "Comment");
            DropColumn("dbo.Item", "DefaultDetailHeaderId");
            DropColumn("Manufacturing.MasterOperation", "ModifiedDate");
            DropColumn("Manufacturing.MasterOperation", "ModifiedBy");
        }
        
        public override void Down()
        {
            AddColumn("Manufacturing.MasterOperation", "ModifiedBy", c => c.Guid(nullable: false));
            AddColumn("Manufacturing.MasterOperation", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Item", "DefaultDetailHeaderId", c => c.Guid());
            AddColumn("dbo.Item", "Comment", c => c.String(maxLength: 4000));
            AddColumn("dbo.Item", "SignalId", c => c.Guid());
            AddColumn("dbo.Item", "ShelfLife", c => c.Int(nullable: false));
            AddColumn("dbo.Item", "ShelfLifePeriodClv", c => c.Guid());
            AddColumn("dbo.Item", "SlowMovingPercent", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "AbcCode", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
            AddColumn("dbo.Item", "IsPurchaseInspectionNeeded", c => c.Boolean(nullable: false));
            AddColumn("dbo.Item", "SafetyTimeDays", c => c.Int());
            AddColumn("dbo.Item", "OrderLeadTimeDays", c => c.Int());
            AddColumn("dbo.Item", "OrderIntervalDays", c => c.Int());
            AddColumn("dbo.Item", "SafetyStock", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "ReorderPoint", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "EconomicOrderQuantity", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "FixedOrderQuantity", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "MaximumOrderQuantity", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "MinimumOrderQuantity", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "OrderQuantityIncrement", c => c.Decimal(precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "OrderMethodClv", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "OrderSystemClv", c => c.Guid());
            AddColumn("dbo.Item", "OutboundMethodClv", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "PurchaseUnitId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "SalesUnitId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "VolumeUnitId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "Volume", c => c.Decimal(nullable: false, precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "WeightUnitId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "Weight", c => c.Decimal(nullable: false, precision: 12, scale: 4, storeType: "numeric"));
            AddColumn("dbo.Item", "EanCode", c => c.Long());
            AddColumn("dbo.Item", "OriginCountryId", c => c.Guid());
            AddColumn("dbo.Item", "LotControlClv", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "StorageUnitId", c => c.Guid());
            AddColumn("dbo.Item", "InventoryUnitId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "TaxId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "ItemGroupId", c => c.Guid(nullable: false));
            AddColumn("dbo.Item", "ItemTypeClv", c => c.Guid(nullable: false));
            AlterColumn("Manufacturing.MasterOperation", "ProcessId", c => c.Guid(nullable: false));
        }
    }
}
