﻿using System.Data.Entity.ModelConfiguration;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	public class TaskConfiguration : EntityTypeConfiguration<Task>
	{
		public TaskConfiguration()
			: this("Manufacturing")
		{
		}

		public TaskConfiguration(string schema)
		{
			ToTable(schema + ".Operation");

			Property(x => x.ParentId).HasColumnName("ParentId").IsRequired().HasColumnType("uniqueidentifier");
			Property(x => x.StartTime).HasColumnName("StartTime").IsOptional().HasColumnType("datetime2");
			Property(x => x.EndTime).HasColumnName("EndTime").IsOptional().HasColumnType("datetime2");

			HasRequired(a => a.Parent).WithMany(b => b.Children).HasForeignKey(c => c.ParentId); // FK_Operation_Parent
		}
	}
}