using System.Data.Entity.ModelConfiguration;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Dal.Manufacturing
{
	public class BaseOperationConfiguration : EntityTypeConfiguration<BaseOperation>
	{
		public BaseOperationConfiguration()
			: this("Manufacturing")
		{
		}

		public BaseOperationConfiguration(string schema)
		{
			ToTable(schema + ".Operation");
			HasKey(x => x.Id);

			Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("uniqueidentifier").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
			Property(x => x.Code).HasColumnName("Code").IsOptional().HasColumnType("nvarchar");
		}
	}
}

