﻿using System.Data.Entity.ModelConfiguration;

namespace Dal.Manufacturing
{
	public class ManufacturingItemConfiguration : EntityTypeConfiguration<ManufacturingItem>
	{
		public ManufacturingItemConfiguration()
			: this("Manufacturing")
		{
		}

		public ManufacturingItemConfiguration(string schema)
		{
			ToTable(schema + ".Item");
			Property(x => x.PurchaseLeadTime).HasColumnName("PurchaseLeadTime")
				.IsRequired().HasColumnType("float");
		}
	}
}