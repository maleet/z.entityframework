using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	public class ProductionOrder
	{
		private ManufacturingItem _item;
		public Guid Id { get; set; }
		public string Code { get; set; }
		public Guid ItemId { get; set; }

		public ManufacturingItem Item
		{
			get { return _item; }
			set
			{
				_item = value;
				if (value != null && value.Id != Guid.Empty) ItemId = value.Id;
			}
		}

		public virtual IList<Operation> Operations { get; set; } = new List<Operation>();

		public List<MasterOperation> MasterOperations { get; set; } = new List<MasterOperation>();

        [NotMapped]
        public double Rank;

        [NotMapped]
		public Operation FirstOperation
		{
			get
			{
				return Operations.OrderBy(operation => operation.MasterOperation == null ? operation.Code : operation.MasterOperation.ProcessNumber.ToString()).FirstOrDefault();
			}
		}

		[NotMapped]
		public Operation LastOperation
		{
			get
			{
				return Operations.OrderByDescending(operation => operation.MasterOperation == null ? operation.Code : operation.MasterOperation.ProcessNumber.ToString()).FirstOrDefault();
			}
		}

		public override string ToString()
		{
			var result = Code;
			if (Item != null)
			{
				result += ", " + Item;
			}
			return result;
		}
	}
}