﻿using System.Data.Entity.ModelConfiguration;
using Dal.Manufacturing;

namespace Nortal.Mes.Dal.Manufacturing.Entities
{
	public class OperationConfiguration : EntityTypeConfiguration<Operation>
	{
		public OperationConfiguration()
			: this("Manufacturing")
		{
		}

		public OperationConfiguration(string schema)
		{
			ToTable(schema + ".Operation");
			Property(x => x.OrderId).HasColumnName("OrderId").IsOptional().HasColumnType("uniqueidentifier");
			Property(x => x.MasterOperationId).HasColumnName("MasterOperationId").IsOptional().HasColumnType("uniqueidentifier");

			HasOptional(a => a.MasterOperation).WithMany(b => b.Operations).HasForeignKey(c => c.MasterOperationId); // FK_Operation_MasterOperation
			HasOptional(a => a.Order).WithMany(b => b.Operations).HasForeignKey(c => c.OrderId); // FK_Operation_ManufacturingOrder

		}
	}
}