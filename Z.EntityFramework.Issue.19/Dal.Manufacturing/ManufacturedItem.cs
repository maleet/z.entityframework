using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	[Table("Item", Schema = "Manufacturing")]
	public class ManufacturedItem : ManufacturingItem
	{
		public virtual List<MasterOperation> MasterOperations { get; set; } = new List<MasterOperation>();
	}

	[Table("Item", Schema = "Manufacturing")]
	public class FinishedItem : ManufacturedItem
	{
	}

	[Table("Item", Schema = "Manufacturing")]
	public class ManufacturedPart : ManufacturedItem
	{
	}
}