using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Dal.Manufacturing
{
	public class MasterOperation
	{
		private ProductionOrder _productionOrder;
		private ManufacturedItem _item;
		public Guid Id { get; set; }
		public string Code { get; set; }

		public Guid? ParentId { get; set; }
		public Guid? ProductionOrderId { get; set; }
		public Guid? ProcessId { get; set; }
		public int? ProcessNumber { get; set; }
		public Guid? ItemId { get; set; }
		public Guid? ItemGroupId { get; set; }
		public Guid? ProcessSelectorId { get; set; }

		public virtual IList<Operation> Operations { get; set; } = new List<Operation>();
		public List<MasterOperation> Children { get; set; } = new List<MasterOperation>();

		public MasterOperation Parent { get; set; }

		public virtual ManufacturedItem Item
		{
			get { return _item; }
			set
			{
				_item = value;
				if (value != null && value.Id != Guid.Empty) ItemId = value.Id;
			}
		}

		public virtual ProductionOrder ProductionOrder
		{
			get { return _productionOrder; }
			set
			{
				_productionOrder = value;
				if (value != null && value.Id != Guid.Empty) ProductionOrderId = value.Id;
			}
		}

		public override string ToString()
		{
			return $"{ProcessNumber}:{Code}";
		}
	}
}