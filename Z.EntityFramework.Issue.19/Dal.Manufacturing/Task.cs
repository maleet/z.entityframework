using System;
using System.ComponentModel.DataAnnotations.Schema;
using Dal.Manufacturing;

namespace Nortal.Mes.Dal.Manufacturing.Entities
{
	[Table("Operation", Schema = "Manufacturing")]
	public abstract class Task : BaseOperation
	{
		public Guid ParentId { get; set; }

		public DateTime? StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		public virtual Operation Parent { get; set; }
	}

	[Table("Operation", Schema = "Manufacturing")]
	public class ProductionTask : Task
	{
		public ProductionTask()
		{
		    Code = "ProductionTask:" + DateTime.Now.ToFileTime();
		}
	}
}