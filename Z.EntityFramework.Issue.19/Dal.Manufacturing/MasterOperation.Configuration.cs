using System.Data.Entity.ModelConfiguration;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Dal.Manufacturing
{
	public partial class MasterOperationConfiguration : EntityTypeConfiguration<MasterOperation>
	{
		public MasterOperationConfiguration()
			: this("Manufacturing")
		{
		}

		public MasterOperationConfiguration(string schema)
		{
			ToTable(schema + ".MasterOperation");
			HasKey(x => x.Id);

			Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("uniqueidentifier").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
			Property(x => x.ProcessNumber).HasColumnName("ProcessNumber").IsRequired().HasColumnType("int");
			Property(x => x.ItemId).HasColumnName("ItemId").IsOptional().HasColumnType("uniqueidentifier");
			Property(x => x.ItemGroupId).HasColumnName("ItemGroupId").IsOptional().HasColumnType("uniqueidentifier");
			Property(x => x.ProductionOrderId).HasColumnName("ProductionOrderId").IsOptional().HasColumnType("uniqueidentifier");
			Property(x => x.Code).HasColumnName("Code").IsRequired().HasColumnType("nvarchar").HasMaxLength(32);

			Property(x => x.ProcessSelectorId).HasColumnName("ProcessSelectorId").IsOptional().HasColumnType("uniqueidentifier");
			Property(x => x.ParentId).HasColumnName("ParentId").IsOptional().HasColumnType("uniqueidentifier");

			HasOptional(a => a.Item).WithMany(b => b.MasterOperations).HasForeignKey(c => c.ItemId);  
			HasOptional(a => a.ProductionOrder).WithMany(b => b.MasterOperations).HasForeignKey(c => c.ProductionOrderId);

			HasOptional(a => a.Parent).WithMany(b => b.Children).HasForeignKey(c => c.ParentId);
		}
	}
}

