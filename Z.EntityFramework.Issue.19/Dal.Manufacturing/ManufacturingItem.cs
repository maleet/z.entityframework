﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Dal.Base.Common;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	[Table("Item", Schema = "Manufacturing")]
	public class ManufacturingItem : Item
	{
		public double PurchaseLeadTime { get; set; } = 6*60; //In minutes

		public virtual List<ProductionOrder> ProductionOrders { get; set; } = new List<ProductionOrder>();
	}

	[Table("Item", Schema = "Manufacturing")]
	public class RawMaterial : ManufacturingItem
	{
	}

	[Table("Item", Schema = "Manufacturing")]
	public class IntermediateItem : ManufacturingItem
	{
	}
}