#pragma warning disable 1591          

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dal.Manufacturing
{
	[Table("Operation", Schema = "Manufacturing")]
	public abstract class BaseOperation
	{
		public Guid Id { get; set; }
		public string Code { get; set; }
	}
}