#pragma warning disable 1591          

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	public class MesEntities : Dal.Base.MesEntities
	{
		static MesEntities()
		{
			Database.SetInitializer<MesEntities>(null);
		}

		public MesEntities()
			: base("Name=DefaultConnection")
		{
		}

		public MesEntities(string connectionString) : base(connectionString)
		{
		}

		public MesEntities(string connectionString, DbCompiledModel model) : base(connectionString, model)
		{
		}

		public DbSet<ProductionOrder> ProductionOrders { get; set; }
		public DbSet<MasterOperation> MasterOperations { get; set; }
		public DbSet<Operation> Operations { get; set; }
		public DbSet<Task> Tasks { get; set; }
		public DbSet<ManufacturingItem> ManufacturingItems { get; set; }
		public DbSet<RawMaterial> RawMaterials { get; set; }
		public DbSet<IntermediateItem> IntermediateItems { get; set; }

        public DbSet<ManufacturedItem> ManufacturedItems { get; set; }
		public DbSet<FinishedItem> FinishedItems { get; set; }
		public DbSet<ManufacturedPart> ManufacturedParts { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Configurations.Add(new ProductionOrderConfiguration());
			modelBuilder.Configurations.Add(new MasterOperationConfiguration());
			modelBuilder.Configurations.Add(new BaseOperationConfiguration());
			modelBuilder.Configurations.Add(new ManufacturingItemConfiguration());
		}
	}
}