using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Nortal.Mes.Dal.Manufacturing.Entities;

namespace Dal.Manufacturing
{
	[Table("Operation", Schema = "Manufacturing")]
	public class Operation : BaseOperation
	{
		private MasterOperation _masterOperation;
		public Guid? OrderId { get; set; }

		public Guid? MasterOperationId { get; set; }

		public virtual ProductionOrder Order { get; set; }

		public virtual MasterOperation MasterOperation
		{
			get { return _masterOperation; }
			set
			{
				_masterOperation = value;
				if (value != null && value.Id != Guid.Empty) MasterOperationId = value.Id;
			}
		}

		public virtual IList<Task> Children { get; set; } = new List<Task>();

	    [NotMapped]
		public int Level { get; set; }
	}
}